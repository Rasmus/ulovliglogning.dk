# Jekyll site for ulovliglogning.dk

## To work on the code, either

### Use docker
You need docker-compose. Simply run
`docker-compose up` to launch the site in a docker container with livereload support. 

### Or install Jekyll locally

#### Install Jekyll and Bundler gems through RubyGems
`gem install jekyll bundler`

#### Install dependencies
`bundle install`

#### Build the site local
`bundle exec jekyll serve`

You can now see the site at [localhost:4000](http://localhost:4000)

## Deploy

It's **important** that you build the site with `JEKYLL_ENV=production bundle exec jekyll build` before deploying the contents of the `_site` directory. We're doing some debugging stuff when then project is build in the development env.

## Structure
The contents of the index page is defined in the frontmatter of the file `index.md`