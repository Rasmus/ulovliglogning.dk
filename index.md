---
layout: indexpage
sections:
  - id: intro
    heading: Lad os stoppe overvågningen!
    content: |
      For mere end et år siden blev den danske totalovervågning af dine internetvaner og bevægemønstre kendt ulovlig ved EU-domstolen. Det bryder de danske politikere sig ikke om, så de har bedt telebranchen fortsætte ulovlighederne. Branchen er splittet. De fire store, TDC, 3, Telenor og Telia logger stadig. Også mere end de må.

      Vi er en broget flok af privatlivsentusiaster og menneskerettighedsforekæmpere der nu går til domstolene med simpelt budskab: Overhold loven og respektér vores fundamentale rettigheder!

      Sammen med vores advokater arbejder vi nu på en udførlig stævning. Den er klar til en høringsfase i starten af marts, bliver offentliggjort d. 15. marts og indleveret til retten d. 2. april 2018.   
  - id: butwhy
    heading: Hvorfor er det vigtigt?
    content: |
      Måske har du ikke noget at skjule i dag, men måske bliver dine ligegyldige data vigtige i morgen. Hvis en bekendt bliver anholdt er du automatisk under mistanke, bare fordi du sendte en nytårs-sms. Måske har du googlet “gødning” eller “TATP”, dagen før en hjemmelavet bombe bliver fundet?

      Måske er du gået forbi et sted hvor der blev begået en forbrydelse. Politiet får en liste over alle der var i nærheden, og så skal du bevise at du ikke var skyldig.

      Dansk politi kan rejse tiltale udelukkende på baggrund af ting du har skrevet i Facebook Messenger. Har du skrevet negative ting om danske politikere på Facebook? Måske bliver du sigtet i næste uge.
  - id: tellmemore
    heading: Jeg vil vide mere…
    content: |
      Sagen startede med en registerindsigt hos teleselskabet TDC. Det var Rasmus Malver der bad om den, og efter lang tids venten og mange rykkere sendte TDC en ukrypteret indsigt. Indsigten viste sig at indeholde ulovligt optagne data, men brancheforeningen Teleindustrien insisterede på at TDC ikke ønskede at logge. Hvis de var tilfældet kunne man lægge sag an imod TDC, og de ville bare erkende at indsamlingen var ulovlig. Rasmus skrev en stævning, som du kan downloade her.

      Men det viste sig at brancheorganisationen ikke havde talt med TDC, for det tidligere statsmonopol ville kæmpe for retten til ulovlig logning. Teleindustrien skiftede holdning, og tilvejebragte et personligt brev fra justitsminister Søren Pape Poulsen, hvori han truer organisationens medlemmer til at fortsætte logning.

      Sagen kan stadig anlægges imod TDC, men med brevet fra ministeren er det lettere for dem at påstå uvidenhed. I samråd med en advokat blev sagen ændret, og målet er nu at få den danske stat dømt for ikke at overholde menneskeretten. Det er en større og dyrere sag, men den har til gengæld potentiale for at ændre politikeres adgang til at ignorere vores fundamentale rettigheder.
  - id: wannahelp
    heading: Jeg vil hjælpe!
    content: |
      Hvis du har lyst til at hjælpe, kan du kontakte [Rasmus Malver](https://twitter.com/rasmusmalver) på [Twitter](https://twitter.com/rasmusmalver) eller på sms.

      I løbet af uge 3 (15. - 21. januar) forventer vi at få et kontonummer, så økonomiske bidrag kan indbetales så anonymt som muligt. Selvom vi har indsamlet 100.000 kr, og dermed har nået det første del-mål, har vi stadig brug for støtte.

      Den danske stats advokat, Kammeradvokat Poul Schmith, har ubegrænsede midler, og den ulige balance forhindrer mange i at tage principielle spørgsmål til domstolene. Vores næste delmål er 250.000 kr. Det skulle gerne få sagen prøvet i første instans.

      Du kan også hjælpe ved at skabe opmærksomhed. Kontakt dit netværk, journalister og din familie, og fortæl hvorfor det er vigtigt at kæmpe for vores basale rettigheder.
  - id: faq
    heading: Spørgsmål & svar
    content: |
      * ### Hvorfor Søren Pape?
        Fordi han er justitsminister. Logning var også ulovligt da Søren Pind, Mette Frederiksen, Karen Hækkerup, Morten Bødskov og Brian Mikkelsen var justitsministre, så det er ikke et spørgsmål om politiske holdninger. Det er et spørgsmål om at respektere fundamentale rettigheder, herunder alles ret til privatliv.

        Sagen vil forhåbentlig ændre danske politikeres åbenlyse og intentionelle overtrædelser af menneskeretten.
      * ### Hvorfor ikke TDC?
        TDC’s påstand er juridisk vildfarelse. Det betyder at de ikke kan stilles til ansvar for at gøre noget ulovligt, fordi de ikke kunne forventes at forstå at det var ulovligt. Med Papes brev til Teleindustrien står de bedre. Men der er stadig en forventning om at man skal kunne indse at en ordre er ulovlig. Også når den kommer fra en minister.

        Det er både hårdere og dyrere at gå efter Justitsministeriet i stedet, men til gengæld kan det ændre retstilstanden i Danmark. Og måske politikernes aktive overtrædelse af vores rettigheder.
      * ### Hvorfor skal jeg bekymre mig om logning? Jeg har intet at skjule, så hvis det hjælper mod kriminalitet går jeg ind for logning!

        Total overvågning hjælper ikke nødvendigvis imod kriminalitet. Det **kan** give flere sigtelser og dømte, men primært fordi flere uskyldige vil blive straffet. Hvis der bliver begået en forbrydelse i en demokratisk retsstat skal politiet og ofrene arbejde sammen for at identificere hvem der kunne have en interesse i at begå forbrydelsen, hvem der havde skaffet sig adgang til gerningsstedet, og hvem der har udvist mistænkelig adfærd.

        Med totalovervågning kan politiet trække en liste over alle personer der var i nærheden af gerningsstedet, og derefter vælge hvem de lettest kan få dømt. Det er derfor dit ansvar at bevise at du ikke har begået en forbrydelse. Anklageren behøver ikke finde et motiv, eller bevise at du har handlet på en bestemt måde. De kan bare vælge dig fra listen over mobiltelefoner der har været i området, eller blandt folk der har googlet “brækjern” 24 timer før.

        Når logning standser bliver politiet ikke forvandlet til mulvarpe. Der er stadig vidtgående muligheder for at overvåge folk på grund af konkret mistanke, men politiet skal igen kunne argumentere for indgrebet. 

      * ### Hvem står bag søgsmålet?

        Menneskeretsjurist [Rasmus Malver](https://twitter.com/rasmusmalver) startede indsamlingen, og den næste store donor var [Bitbureauet](https://bitbureauet.dk/). Derfra tog det fart, og mere end 20 andre personer, virksomheder og foreninger har doneret til sagen. De indsamlede penge “tilhører” en forening hvis eneste formål er at føre retssagen og at sprede budskabet. Du kan læse vedtægterne her.

        Foreningen har valgt IT- og EU-retsspecialistkontoret [Bird & Bird](https://www.twobirds.com), hvor advokat Martin von Haller er primær tovholder. 

      * ### Hvad har logning med menneskeret at gøre?

        Menneskeret er dine rettigheder overfor stater. I nogle lande fremgår de af forfatningen, men i Danmark er de primært kommet fra Den Europæiske Menneskerettighedskonvention ([pdf](http://www.echr.coe.int/Documents/Convention_ENG.pdf)). Den blev skrevet efter 2. verdenskrig og er løbende blevet opdateret, for at undgå en gentagelse af Nazityskland og Østblokkens rædsler. I år 2000 skrev EU et Charter om Grundlæggende Rettigheder ([pdf](http://www.europarl.europa.eu/charter/pdf/text_da.pdf)) der indgår på overstatsligt niveau, dog kun for emner omfattet af EU-samarbejdet.

        Begge konventioner indeholder en beskyttelse af dit privatliv, og det er slået klart fast at staten ikke må overvåge alle konstant. Men det gør Danmark. 

        På grund af logningen ved staten altid hvor din mobil er, om du er på nettet, og hvem du kommunikerer med. Der indsamles mere information om dig og din adfærd end STASI og Gestapo nogensinde kom i nærheden af.
        Du har ret til at være fri for dén overvågning.

      * ### Tjener i nogle penge på det her? Hvad hvis der er penge i overskud?

        Nej. Pengene går til at betale advokatkontoret ([Bird & Bird](https://www.twobirds.com)) og til at betale sagsomkostninger. Hvis der er penge “til overs” vil de gå til informationsmateriale om logning og/eller til en non-profit-organisation med samme formål.

      * ### Hvad er det egentligt der bliver logget om mig?

        Hvor din mobil er på alle tidspunkter af døgnet, hvem du kommunikerer med og i et vist omfang hvad du laver på nettet. Du kan bede din udbyder sende dig en kopi af alt hvad de har registreret. Det kan koste op til 200 kr.

      * ### Ministeren siger at han skal bruge tid på at ændre lovgivningen, det er vel fair nok?

        Nej. Allerede da totalovervågningen blev indført fik den danske stat at vide at det ville være ulovligt.

        En lov og en bekendtgørelse kan være ulovlige, hvis de eks. strider imod en overstatslig regel, i dette tilfælde EU-Charteret, eller hvis de underforstået ønsker at overholde Menneskerettighedskonventionen.

        Ved Digital Rights-dommen blev det slået fast at totalovervågning var i strid med Charteret, og i Tele2/Watson-dommen blev det slået fast igen. Intet af dette har været en overraskelse, og dommene betyder “kun” at staten ikke kan bruge uvidenhed som undskyldning.

        Justitsministeriet ønsker at indføre en ny form for overvågning. Det er deres ret at foreslå ny lovgivning, men det betyder ikke at man kan opretholde en igangværende forbrydelse. 

        Hvis man går over for rødt må man ikke standse midt i krydset, og blive stående indtil man har opfundet en jetpack så man i fremtiden kan flyve over for rødt. Hvis man laver et bankrøveri og alarmen går, kan man ikke tage gidsler mens man prøver at finde på en plan for sit næste bankrøveri.

        Logning er en kriminel handling og den eneste grund til at justitsministeren ikke sidder i fængsel, er at kun Folketinget kan stille ham foran en dommer.
---