---
layout: indexpage
lang: en
permalink: /
sections:
  - id: intro
    heading: Lets stop the mass surveillance!
    content: |
      The mass surveillance conducted by European governments has on two occasions been struck down by the European Court of Justice. Most recently this happened in 2016, when the court reviewed the joint case of Tele2 from Sweden and Tom Watson et al from England and Wales. However Danish politicians and telco industry refuse to honour fundamental rights, and continue to retain data illegally.

      We are a motley crew of human rights advocates and privacy campaigners that have come together to use the courts to send a simple message: Honour the law and respect our fundamental rights.

      In cooperation with our attorneys we are currently working on the suppœna to file suit against the minister of justice, Søren Pape Poulsen. The document will be published 15 March for public consultation, and filed on 2 April 2018.   
  - id: butwhy
    heading: But why?
    content: |
      You might not think that your secrets are worth keeping, but with enough data, everyone's a suspect. Maybe you've sent a merry christmas-text to the suspect of an ongoing investigation? Maybe you've googled “manure” the week before a homemade bomb is found?

      Maybe you're the only person with a mobile phone that's been near the scene of a crime, and maybe you'd just bought binliners. Mass surveillance flips the burden of proof. Your movements are recorded and can be used against you, but not in your defence. You might have left your phone at home on purpose.

  - id: tellmemore
    heading: Tell me more!
    content: |
      The movement began when Rasmus Malver asked his phone company, TDC, for a copy of their retained data. When the company finally delivered the data, it was evident that they had retained data illegally. The Telecom Industry Association of Denmark had publically claimed that they were opposed to violating human rights. Malver decided to call their bluff, and when presented with a draft suppœna TDC disowned the association's statement.
      	
      Attempting to justify their illegal actions, the Telecom Association published a letter from the minister of justice, Søren Pape Poulsen, telling them to disregard the law. 
			
      The case could still be brought against the phone companies, but with this letter they have a better case for arguing ignorance (although that is [technically a moot point](https://en.wikipedia.org/wiki/Ignorantia_juris_non_excusat)). However the actions of the minister are so grave that, in consultation with our attorneys, we have decided to file suit against the state instead. A democratic society cannot tolerate officials threatening citizens and companies with prosecution if they honour the law.
  - id: wannahelp
    heading: I want to help!
    content: |
      You can contact spokesperson [Rasmus Malver](https://twitter.com/rasmusmalver) on [Twitter](https://twitter.com/rasmusmalver) or via [sms/signal](sms:+4526809424).

      You can also transfer money to `IBAN: DK6850420001165817`, `SWIFT: JYBADKKK` with the message `RASMA.0001` (important). We have already reached our first strech goal, 100.000 DKK, but we still need your help!
			
      The designated attorney for the Danish government, Kammeradvokaten, has access to unlimited means. When your main customer prints their own money, you are free to charge a substantial sum.

      You can also help us gain momentum through publicity. Contact your network, journalists, friends and family - and explain why it is important to fight for our rights.
  - id: faq
    heading: Questions & answers
    content: | 
      * ### Why Søren Pape?
        Because he is the relevant minister. Data retention was equally illegal when Søren Pind (V), Mette Frederiksen (S), Karen Hækkerup (S), Morten Bødskov (S) and Brian Arthur Mikkelsen (K) held the office. It is not a political issue. It is about hounouring the law and not violating the fundamental rights.

        This case will, hopefully, change Danish politicians obvious and intentional violations of our human rights.

      * ### Why not TDC?
      TDC claim ignorance of the law. With the letter from the minister, they argue that they lack the ability to understand the illegality of a ministerial order.

        It will be more difficult and more expensive to argue a case against the state of Denmark. But it will set the precedent for future governments wanting to oppress the fundamental rights of the people.
      
      * ### Why should I care? If surveillance can hinder crime, I'm pro data retention!
        Mass surveillance does not necessarily prevent or stop crime. It **can** lead to more arrests and fewer unsolved crimes, but mainly because innocent people will be punished. Having a database table of the location of unsolved crime and a database table of everybody's movements makes it possible to collate data and prove that everybody is guilty.

        Have you bought a crowbar recently or have you walked past a house that has been burgled? Mass surveillance requires you to [doublethink](https://en.wikipedia.org/wiki/Doublethink) to stay out of jail.

        Honouring human rights does not leave the police blind as bats. Crime could be solved before 2006, but the police have to think for themselves. Who might be a suspect and why? It will still be possible to conduct surveillance, but only with sufficient democratic oversight.

      * ### Who's behind this?

        Human rights jurist [Rasmus Malver](https://twitter.com/rasmusmalver) kickstarted the fundraising with a contribution of 30.000 DKK, and the second large donor is [Bitbureauet](https://bitbureauet.dk/). In January the movement  gained traction and more than 20 people, businesses and organisation brought the total to 100.000 DKK. An organisation was created to hold the money, and it will all be used to pay the legal fees.
      
        We have chosen IT- og EU-specialists [Bird & Bird](https://www.twobirds.com), and advokat Martin von Haller leads the team. 

      * ### What is the relationship to human rights?

        Human rights are your rights againt governmental abuse. Some countries have human rights enshrined in their constitutions, but in Denmark they mainly exist in the form of the European Convention on Human Rights ([pdf](http://www.echr.coe.int/Documents/Convention_ENG.pdf)). It was written after the second world war to avoid history repeating, and over time it has been updated to protect minorities and to avoid the emerging terrors from states on both sides of the iron curtain.

        The people of the European Union has agreed upon an updated version of the convention in 2000, the EU Charter on Fundamental Rights ([pdf](http://www.europarl.europa.eu/charter/pdf/text_en.pdf)), where the protection of privacy is emphasised.

        The Danish mass surveillance and retention of metadata is an obvious violation of both conventions, and it has been established that both are important parts of Danish law. The current surveillance is more intrusive than the surveillance conducted by both Gestapo and STASI.

        You have the right not be the victim of this.

        * ### Are you profiting from this? What will happen if there's too much money?

        No. All of the funds will go to paying legal fees and the attorney, ([Bird & Bird](https://www.twobirds.com)). If there is “too much” money (unlikely), it will be forwarded to a similar case or organisation.

        * ### What is being retained about me?

        The location of your mobile phone at all times of every day. Who you communicate with and, to some degree, what you do online. You can ask your provider for a copy of the data. If they reply within a reasonable time, they are allowed to charge you up to 200 DKK for it.

        * ### The minister says he need time to replace the legislation. Isn't that ok?
        No. When mass surveillance was brought in on EU level, politicians were told it violates human rights, and thus it would be illegal. When implemented in Denmark they were told the same. When the ministerial notice was issued they were told again.

        A ministerial notice (bekendtgørelse) cannot exist without a legal basis in a law, and a law implementing EU regulation cannot exist when the regulation has been struck down.
      
        The European Court of Justice was unusually clear when striking down the surveillance regulation in both the case of Digital Rights and Tele2/Watson. It did not come as a surprise to anybody, and obviously it withdrew any legal basis from the ministerial notice.

        Justice minister Søren Pape Poulsen wants to continue surveillance, and is desperately looking for a way to sneak it through parliament. There is a large majority for doing it, but it would require Danish secession from the Council of Europe and potentially the European Union. Denmark would then stand with Belarus as one of only two European states not a party to the Convention on Human Rights.

        Metadata retention is a criminal act, and the minister is only kept out of jail by constitutional provision stating that only other politicians can bring him to justice.
---